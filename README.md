# IoT_workshop

## Getting Started

### Prerequisites

Ubuntu 18.04

### Install nodejs

Install Curl
```
sudo apt install curl
```
Run commands:
```
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt-get install -y nodejs
```
Verify node version
```
node -v
```

[SOURCE](https://github.com/nodesource/distributions/blob/master/README.md)

### Install node-red
Run command:
```
sudo npm install -g --unsafe-perm node-red
or

sudo snap install node-red
```

### Install Mosquitto
Run command:
```
sudo apt install mosquitto mosquitto-clients
```

### Install MongoDB
[MongoDB](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/)
```
In case of problems with mongod.service run: "sudo systemctl enable mongod.service"
```

### Install Visual Code
Download [here](https://code.visualstudio.com/download)

Run
```
sudo dpkg -i code_1.40.0-1573064499_amd64.deb
```

### Install extension REST
Rest Client of Huachao Mao

### Create .env file in dir
Copy to the file:
```
DATABASE_URL=mongodb://localhost:27017/sensors
```

### Commands
Node red
```
node-red

```
Mongodb:
```
sudo service mongod start

mongo

sudo service mongod stop
```
Mosquitto:
```
mosquitto

sudo systemctl stop mosquitto
```

### Starting 
Commands for a new project(nodejs):
```
npm init

npm i express mongoose
npm i --save-dev dotenv nodemon
```

Change package.json script to:
```
"scripts": {
    "iotstart": "nodemon server.js"
  },
```

Run program:
```
npm run iotstart
```
### Other Installation

For PyProject 

```
sudo apt git
sudo apt python
sudo apt install python-pip
pip install pyserial
```

For EuroAge Project
```
Install MongoDB Community Edition
Install MongoDB Compass

In case of problems with mongod.service run: "sudo systemctl enable mongod.service"
```
Setting up the Server (If authentication required):
```
https://medium.com/founding-ithaka/setting-up-and-connecting-to-a-remote-mongodb-database-5df754a4da89

sudo service mongod start

Log in if authentication applied: mongo -u useradmin  127.0.0.1/admin
```
To make request just run the cmd to get the internal ip:
```
ip addr show
```

## Authors
* **Joao Falcao**

## Acknowledgments
* **Web Dev Simplified** - *REST API template* - [WebDevSimplified](https://www.youtube.com/channel/UCFbNIlppjAuEX4znoulh0Cw)
* **Billie Thompson** - *Read.me template* - [PurpleBooth](https://github.com/PurpleBooth)



