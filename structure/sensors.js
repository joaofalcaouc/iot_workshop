const express = require('express')
const router = express.Router()
const Sensor = require('../storage/sensor')

//REST API

//GET ALL ENTRIES
router.get('/', async(req,res) => {
    try {
        const sensors = await Sensor.find()
        res.json(sensors)
    } catch(err) {
        res.status(500).json({message: err.message})
    }
})

//GET ONE
router.get('/:id', getSensor, (req,res) =>{
    res.json(res.sensor)
})

//CREATE ONE
router.post('/', async (req,res) =>{
    const sensor = new Sensor({
        name: req.body.name,
        timestamp: req.body.timestamp
    })
    try {
        const newSensor = await sensor.save()
        res.status(201).json(newSensor)
    } catch (err) {
        res.status(400).json({message: err.message})
    }
})

//UPDATE ONE
router.patch('/:id', getSensor, async (req,res) =>{
    if(req.body.name != null){
        res.sensor.name = req.body.name
    }
    res.sensor.timestamp = Math.floor(Date.now()/1000)
    try {
        const updatedSensor = await res.sensor.save()
        res.json(updatedSensor)
    } catch (err){
        res.status(400).json({message: err.message })
    }
})

//DELETE ONE
router.delete('/:id', getSensor, async (req,res) =>{
    try {
        await res.sensor.remove()
        res.json({ message: 'Deleted Sensor Data'})
    } catch (err) {
        res.status(500).json({ message: err.message })
    }
})

//Search by id
async function getSensor(req,res,next){
    let sensor
    try {
        sensor = await Sensor.findById(req.params.id)
        if(sensor == null){
            return res.status(404).json({ message: 'Cannot find sensor'})
        }
    } catch (err) {
        return res.status(500).json({ message: err.message})
    }
    res.sensor = sensor
    next()
}

module.exports = router