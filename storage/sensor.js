const mongoose = require('mongoose')

const sensorSchema = new mongoose.Schema({
    //Here you can define which parameter of your database

    name:{
        type: String,
        required: true
    },
    timestamp:{
        type: Number,
        required: true,
        default: Math.floor(Date.now()/1000)
    }
})

module.exports = mongoose.model('Sensor', sensorSchema)